import type { IncomingMessage, ServerResponse, Server } from "http";
import adminjsApp from "../../adminjs";

export default (
  req: IncomingMessage,
  res: ServerResponse,
  next: () => void,
) => {
  return new Promise(async (resolve) => {
    if (!req.url?.startsWith("/admin")) {
      return next();
    }
    const app = await adminjsApp;
    const server: Server = app.getHttpServer();
    const [requestHandler] = server.listeners("request");
    requestHandler(req, res);
    res.on("finish", resolve);
  });
};
