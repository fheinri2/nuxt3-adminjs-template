import { validate } from "class-validator";
import { AdminModule as NestJsAdminModule } from "@adminjs/nestjs";
import { Database, Resource } from "@adminjs/typeorm";
import { getRepositoryToken, TypeOrmModule } from "@nestjs/typeorm";
import AdminJS from "adminjs";
import { NestFactory } from "@nestjs/core";
import ormconfig from "../ormconfig";
import { getResources } from "../entities";

// import { Module } from "@nestjs/common";
import pkg from "@nestjs/common";
const { Module } = pkg;

Resource.validate = validate;
AdminJS.registerAdapter({ Database, Resource });

const resources = getResources();

@Module({
  imports: [
    TypeOrmModule.forRoot(ormconfig),
    NestJsAdminModule.createAdminAsync({
      imports: [TypeOrmModule.forFeature(resources)],
      inject: resources.map((resource) => getRepositoryToken(resource)),
      useFactory: () => ({
        auth: {
          authenticate: (_email, _password) =>
            Promise.resolve({ email: "test@test.com" }),
          cookieName: "test",
          cookiePassword: "testPass",
        },
        adminJsOptions: {
          rootPath: "/admin",
          resources,
        },
      }),
    }),
    // NestJsAdminModule.createAdmin({
    //   adminJsOptions: {
    //     rootPath: "/admin",
    //     resources,
    //   },
    //   auth: {
    //     authenticate: (_email, _password) => Promise.resolve({ email: "test" }),
    //     cookieName: "test",
    //     cookiePassword: "testPass",
    //   },
    // }),
  ],
})
export class AdminModule {}

const bootstrap = async () => {
  const app = await NestFactory.create(AdminModule);
  await app.init();
  return app;
};
const adminjsApp = bootstrap();

export default adminjsApp;
