import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { getResources } from "./entities";

import dotenv from "dotenv";
dotenv.config();

const DATABASE = process.env.DATABASE || ":memory:";

const ormconfig: TypeOrmModuleOptions = {
  type: "sqlite",
  database: DATABASE,
  synchronize: true,
  logging: true,
  entities: getResources(),
  migrations: ["migrations/**/*.[j,t]s"],
};

export default ormconfig;
