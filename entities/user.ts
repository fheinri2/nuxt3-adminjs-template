// import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import pkg from "typeorm";
const { BaseEntity, PrimaryGeneratedColumn, Column, Entity } = pkg;

@Entity("Users")
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number | undefined;

  @Column({ type: "varchar" })
  name: string | undefined;
}

export default User;
