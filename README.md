# Nuxt3 AdminJS Template

This template combines [Nuxt3](https://v3.nuxtjs.org) and [AdminJS](https://adminjs.co).

This template / example uses the following setup:

- [Nuxt3](https://v3.nuxtjs.org)
- [AdminJS](https://adminjs.co)
- [NestJS](https://nestjs.com)
- [TypeORM](https://typeorm.io)
- [SQLite](https://sqlite.org)

## Setup

Make sure to install the dependencies

```bash
yarn install
```

Copy and edit the local environment configuration file

```bash
cp .env.example .env
${EDITOR:-vi} .env
```

## Development

Start the development server on http://localhost:3000

```bash
yarn dev
```

## Production

Build the application for production

```bash
yarn build
yarn start
```

## AdminJS

AdminJS is accessible on http://localhost:3000/admin with the credentials:

```
Email: test@test.com
Password: testPass
```
